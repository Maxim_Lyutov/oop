<?php
class NewsClass
{
    public $title;
    public $textNews;
    public $publicationDate;
    
    public function __construct($title, $txtNews, $pDate )
    {
        $this->title = $title;
       $this->textNews = $txtNews;
       $this->publicationDate = $pDate;
    }
    
    public function getTitle()
    {
        return $this->title; 
    }
    
    public function getTextNews()
    {
        return $this->textNews; 
    }
    
    public function getPublicationDate()
    {
        return $this->publicationDate; 
    }
}

$news[1] = new NewsClass('На фабрике в Китае роботы начнут делат роботов',
'Крупная компания ABB, которая специализируется в области информационных технологий,
собирается построить «самую передовую фабрику роботов в мире» в Шанхае. 
Предприятие планируют запустить к концу 2020 году.'
,'04/11/2018');

$news[2] = new NewsClass('Стал известен самый мощный Android-смартфон в мире',
'На протяжении нескольких месяцев в топе списка самых мощных в мире смартфонов по версии разработчиков 
бенчмарка AnTuTu были гаджеты на базе процессора Qualcomm Snapdragon 845. 
В октябре этого года ситуация изменилась, и верхние строчки рейтинга занимают смартфоны Huawei, 
базирующиеся на чипе Kirin 980.Согласно свежим данным от создателей AnTuTu, первые три места в рейтинге наиболее мощных смартфонов 
заняли Huawei Mate 20 (312 тысяч баллов), Huawei Mate 20 Pro (307 тысяч баллов)и Huawei Mate 20 X (303 тысячи баллов).'
,'02/11/2018');


$news[3] = new NewsClass('Стало известно, когда LG покажет телефон со сгибающимся экраном', 
'В настоящее время производители не раскрывают подробности проекта, 
но эксперты предрекают внедрение топовых составляющих в очередную новинку. 
Стоит отметить, что южнокорейская компания Samsung анонсировала выход гибкого смартфона ранее. 
Правда, стоимость гаджета пока не известна.'
,'01/11/2018');

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta charset="utf-8">
      <title>Новости ...</title>
      <style>
        .title {
            font-style : italic;
            font-size: 2.0em;
            margin: inherit;
            line-height: 1.4;
        }
        .comment {
              font-style: italic;
              font-size: 1.4em;
              margin: inherit;
              line-height: 1.4;
        }
        .data {
              font-size: .8em;
              line-height: 1.2;
              margin: 15px auto;
        }
        p {
            margin: 0 0 0 3em;
            line-height: 2;
            font-size: 1.1em;
        }
         
      </style>
  </head>
  <body>
     <h2> Новости </h2>
     <hr>
     <h3> Сейчас читают: </h3>
     <div>
    
    <?php foreach($news as $newsOne): ?>
    <p class='title'><?= $newsOne->getTitle(); ?></p>
    <p class='comment'><?= $newsOne->getTextNews(); ?> </p>
    <p class='data'><?= 'Дата публикации: ' . $newsOne->getPublicationDate(); ?></p>
    
    <?php endforeach;?>
    
    </div>
  </body>
</html>
